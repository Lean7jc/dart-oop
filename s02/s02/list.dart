void main(){
//list <datatype> varName = values
  List<int> discountRanges = [20, 40, 60, 80 ];
  List <String> names = ['John','Jane', 'Tom']; 
  const List <String> maritalStatus = ['Single', 'Married', 'Divorced', 'widowed'];

  print(discountRanges);
  print(names);

  print(discountRanges[2]);
  print(names[2]);
  print(names.length);

  names[0] = 'Jonathan';
  print(names);
  names.add('Mark');
  print(names);
  names.insert(3,'Roselle');
  print(names);

  print(maritalStatus);

  print(names.isNotEmpty);
  print(names.length == 0);
  print(names.first);
  print(names.last);
  print(names.reversed);
  
  names.sort();
   print(names.reversed);

  //names.sort((a, b) => a.length.compareTo(b.length));
  //print(names);
}
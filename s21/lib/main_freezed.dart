import './freezed_models/user.dart';

void main( ){
 
    User userA = User(
        id: 1, 
        email: 'john@gmail.com', 
    ); 

    User userB = User(
        id: 1, 
        email: 'john@gmail.com', 
    ); 

    // print(userA.hashCode);
    // print(userB.hashCode);
    // print(userA == userB);

    // demonstration of object immutability below.
    // immutability means changes are not allowed.
    // immutability it ensures that an object will not be changed accidentally

    // instead of directly changin the object's property,
    // the object itself will be changed or re-assigned with new values
    // to achieve this; we use object.copyWith() method.

    // print(userA.email);
    // userA = userA.copyWith(email: 'john@hotmail.com');
    // print(userA.email);

    var response = {'id': 3, 'email': 'doe@gmail.com'};

    // User userC = User(
    //     id: response['id'] as int,
    //     email: response['email'] as String
    //     );

    User userC = User.fromJson(response);
    print(userC);
    print(userC.toJson());
}


import 'package:freezed_annotation/freezed_annotation.dart';

//the user of the contents for user.dart can be
// found in user.freezed.dart using the "part" keyword
part 'user.freezed.dart';
part 'user.g.dart';

// the @freezed annotationt tells the build_runner to
// created freezed class name_$user in user.freezed.dart
@freezed
class  User with _$User {
  const factory User({
      required int id,
      required String email,
      String? accessToken,
    }) = _User;

    factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}
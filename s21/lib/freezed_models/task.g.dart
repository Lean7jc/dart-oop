// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Task _$$_TaskFromJson(Map<String, dynamic> json) => _$_Task(
      id: json['id'] as int,
      userId: json['userId'] as int,
      descrption: json['descrption'] as String,
      imageLocation: json['imageLocation'] as String?,
      isDone: json['isDone'] as int,
    );

Map<String, dynamic> _$$_TaskToJson(_$_Task instance) => <String, dynamic>{
      'id': instance.id,
      'userId': instance.userId,
      'descrption': instance.descrption,
      'imageLocation': instance.imageLocation,
      'isDone': instance.isDone,
    };

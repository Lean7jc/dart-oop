class User {
    late final int? id;
    late final String? email;
    late final String? accesstoken;

    User({
        this.id,
        this.email,
        this.accesstoken
    });
}


class Task{
    late final int id;
    late final int userId;
    late final String descrption;
    late final String? imageLocation;
    late final int isDone;

    Task({
        required this.id,
        required this.userId,
        required this.descrption,
        this.imageLocation,
        required this.isDone,
    });
}
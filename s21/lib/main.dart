import './models/user.dart';

void main( ){
    // String x = '123';
    // String y = '123';

    // print(x.hashCode);
    // print(y.hashCode);
    // print(x == y);

    User userA = User(
        id: 1, 
        email: 'john@gmail.com', 
    ); 

    User userB = User(
        id: 1, 
        email: 'john@gmail.com', 
    ); 

    
    // print(userA == userB);
    // print(userA.hashCode);
    // print(userB.hashCode);

    // to check for object equality, we need to
    // do it like the code below.

    // bool isIdSame = userA.id == userB.id;
    // bool isEmailSame = userA.email == userB.email;
    // bool areObjectSame = isIdSame && isEmailSame;

    // print(areObjectSame);

    print(userA.email);
    userA.email ='john@hotmail.com';
    print(userA.email);

}

//Dart does not check the values of the object 
//but also its  unique identifier(through hash codes).
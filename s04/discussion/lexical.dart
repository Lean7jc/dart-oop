void main(){
  // String firstName = 'John';

  Function discountBy25 = getDiscount(25);
   Function discountBy50 = getDiscount(50);
  print(discountBy25(1400));
  print(discountBy50(1400));

  print(getDiscount(25)(1400));
  print(getDiscount(50)(1400));
}
Function getDiscount (num percentage){
  return(num amount){
    return amount * percentage /100;
  };
}
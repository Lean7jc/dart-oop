

void main (){
  String companyName = getCompanyName();
  print(getCompanyName());
  print(companyName);
  print(getYesEstablishment());
  print(hasOnlineClass());
  print(getCoordinates());
  print(combineAddress('134 Timong Ave.', 'Brgy. Sacred Heart', 'Q.C.', 'Metro Manila'));
  print(combineName("John", 'Smith'));
  print(combineName("John", 'Smith', isLastNameFirst: true));
  print(combineName("John", 'Smith', isLastNameFirst: false));

  List<String> person = ['John Doe', 'Jane DOe'];
  List<String> students = ['Nicholas Rush', 'James Holden'];


 // students.forEach((String person){
  //  print(person);
  //});


  person.forEach(printName);
  //students.forEach(printName);
  print(isUnderAge(18));
}

void printName(String name){
  print(name);
}


//optional named Parameters
//these are parameter added after the required ones.
// there parameter are added inside a curly bracket
// 
String combineName( firstName,  lastName, {bool isLastNameFirst = false}){
  if (isLastNameFirst) {
   return '$lastName $firstName';
  
}else {
   return '$firstName $lastName';
}
}

String combineAddress( specifics,  barangay,  city,  province,){
return '$specifics, $barangay, $city, $province';
}

String getCompanyName(){
        return 'FFUF';
}

int getYesEstablishment(){
  return 2021;
}

bool hasOnlineClass(){
  return true;
} 
/* bool isUnderAge(int age){
   return(age <18 )? true : false;
 } */

 bool isUnderAge(int age) => (age <18)? true:false;
Map<String, dynamic> getCoordinates(){
  return{
    'latitude' : 14.63,
    'longitue' : 121.04
  };
}



void main(){
  Equipment equiment = new Equipment();
  equiment.name = 'Equipment-001';

    Loader loader = new Loader();
  loader.name = 'Loader-001';
  print(loader.name);
  print(loader.getCategory());
  loader.moveForward(10);
  loader.moveBackward();
  print(loader.acceleration);

  Car car = new Car();
  car.name = 'Car-001';
  car.moveForward(10);
  car.moveBackward();
  // print(crane.name);
  // print(crane.getCategory());

}

class Equipment{
  String? name;
}

class Loader extends Equipment with Movement{
  String getCategory(){
    return'${this.name} is a loader.';
  }
}

class Car with Movement, Engine{
  String? name;
  String getCategory(){
    return'${this.name} is a car';
  }
}

mixin Movement{
  num? acceleration;
  void moveForward(num acceleration ){
    this.acceleration = acceleration;
    print('The Vehicle is moving forward');

  }

  

  void moveBackward(){
    print('The Vehicle is moving backward');
  }
}

mixin Engine{
    void start(){

    }
    void stop(){
      
    }
  }
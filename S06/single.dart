void main(){
  Person person = new Person(firstName: 'John', lastName: 'Smith');
  Employee employee = new Employee(lastName: 'Westwood', firstName: 'Jonas', employeeID: '001');
  print(person.getFullname());
  print(employee.getFullname());
}
class Person{
  String firstName;
  String lastName;

  Person({
    required this.firstName,
    required this.lastName
  });

  String getFullname(){
    return '${this.firstName} ${this.lastName}';
  }
}

class Employee extends Person{

  String employeeID;
  Employee({
    required String firstName,
    required String lastName,
    required this.employeeID
    }) : super(  firstName: firstName, lastName: lastName);
}


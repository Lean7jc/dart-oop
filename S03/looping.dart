

void main() {
modifiedForLoop();
forInLoop();
  
    
  }
  
  String selectSector(int sectorID) {
  String name;
  switch(sectorID) {
    case 1: 
      name = 'Craft';
      break;
    case 2:
      name = 'Assembly';
      break;
    case 3:
      name = 'Building Operations';
      break;
    default:
      name = sectorID.toString() + ' is out of bounds';
      // break; optional
  }
  return name;
}/*
  void whileLoop () {
    int count = 5;

    while count !=;
    {
      print(count);
      count--;
    }
    
  }
*/

void forLoop() {
  for (int count = 0; count <= 20; count++) {
    print(count);
  }
}

void whileLoop() {
  int count = 5;
  while (count != 0) {
    print(count);
    count--;
  }
}
void forInLoop() {
  List<Map<String, String>> persons = [
    {'name': 'Brandon'},
    {'name': 'John'},
    {'name': 'Arthur'},
  ];
  for (var person in persons) {
    print(person['name']);
  }
}


void modifiedForLoop(){
  for (int count = 0; count <= 20; count++){
    if(count % 2 ==0){
      continue;
    }
    else{
     print(count);
    }
    if (count >10){
      break;
    }
  }
}
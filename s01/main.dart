//keyword "void" mean function will not return anything
//the syntax of a function is: return type function name(parameters){}
void main(){
    String fname = 'John';
    String? middleName = null;
    String lname = 'smith';
    int age = 31;
    double height = 172.45;
    //name = 1.toString();
    num weight = 64.32; //can accept numbers with our without decimal points
    bool isRegistered = false;

    var arr = [];
    var obj = {};
    List<num> grades = [98.2, 89, 87.88, 91.2]; // list is an array
    //Map person = new Map(); // Map in an object
    Map<String, dynamic> personA = {
      'Name': 'Brandon',
      'Batch': 213
    };
    
    //alternative syntax for declaring object 
    Map<String, dynamic> personB = new Map();
    personB['Name'] = 'Juan';
    personB['Batch'] = 89;
 
    final DateTime now;
    now = DateTime.now();

    const String companyAcronym = 'ffuf';

    //console.log
    //system.out.print
    //print('Hello world');
    //print('Hello ' + fname); //hello is concatenated with the name variable.concatenation
    //print(fname + ' ' + lname);
    print('Fullname: $fname $lname');
    print('Age: ' + age.toString());
    print('Height: '+ height.toString());
    print('Weight: '+ weight.toString());
    print("Register: " + isRegistered.toString());
    print('Grades: ' + grades.toString());
    //print(personA);
    print("Current Datetime: " + now.toString());
    print(personB['Name']);
    
} 
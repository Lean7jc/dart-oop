void main (){
    String firstName = 'John';

    Building building = new Building(
      name: 'Caswynn', 
      floors: 8,
      address: '134 Timog Avenue, Brgy. Sacred Heart, Q.C., Metro Manila', 
      rooms: ["Tech", 'Dev']);

    
    building.name = 'Caswynn';
    building.floors= 8;
    building.address = '134 Timog Avenue, Brgy. Sacred Heart, Q.C., Metro Manila';
    // print(building);
    // print(building.name);
    // print(building.floors);
    // print(building.address);

    }

  class Building{
   String? name;
   int floors;
   String address;
   List <String> rooms;

  Building({
  this.name, 
  required this.floors,
  required this.address,
  required this.rooms})
  {  print('A building object has been created');
} 
}


//before nullable
// class Building{
//    String name;
//    int floors;
//    String address;

//   Building(String name, int floors, String addreess){
//     this.name = name;
//     this.floors = floors;
//     this.address = address;
//   }
// }

//the class declaration looks like this:
/*
class Classname{
  <fields>
  <constructor>
  Method
  gte/set
}
*/
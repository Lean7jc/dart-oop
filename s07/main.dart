

import './worker.dart';
void main () {
  //Person personA = new Person();

  Doctor doc1 = new Doctor(firstName: 'John', lastname: 'Smith');
  
  Carpenter carpenter = new Carpenter();
  print(carpenter.getType());
}

abstract class Person{
  //The class person defines that a "getFullName" must be implemented.
  // however it does not tell the concrete class HOW to implement it.
    String getFullName();
}
// the concrete class the doctor class.
class Doctor implements Person {
   String firstName;
   String lastname;

   Doctor ({
     required this.firstName,
     required this.lastname
   });
  String getFullName() {
    return 'Dr. ${this.firstName} ${this.lastname}';
  }
}

class Attorney implements Person{
  String getFullName(){
    return'Atty.';
  }
}